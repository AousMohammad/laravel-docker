
### Install Docker and Docker Compose on your system.

  

In the directory containing the Dockerfile and Docker Compose file, they should run:

  

`docker-compose build`

  

After the build process completes, you can start all the containers with:

  

`docker-compose up -d`

  

This will start the containers in the background and leave them running.

  

Now, you need to run the Laravel commands inside the PHP container:

  

Get the container ID with `docker ps`. look for the container ID that corresponds to the PHP service (the container running the `app` service).



#### create .env file in main directory and put this options:
`DB_CONNECTION=mysql`


`DB_HOST=db`


`DB_PORT=3306`


`DB_DATABASE=news_app`


`DB_USERNAME=root`


`DB_PASSWORD=password`


  ### Then for each of the Laravel commands, they should run something like this (replace {container_id} with the actual container ID)::

`docker exec -it {container_id} composer install`

`docker exec -it {container_id} php artisan migrate:fresh --seed`

`docker exec -it {container_id} php artisan optimize`

`docker exec -it {container_id} php artisan fetch:articles`


  

You should now be able to access your Laravel application by opening a web browser and going to http://localhost:8000.
