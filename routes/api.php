<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/register', [\App\Http\Controllers\AuthController::class, 'register']);
Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login']);
Route::middleware(['auth:sanctum','api'])->group(function () {
    Route::post('/logout', [\App\Http\Controllers\AuthController::class, 'logout']);
    Route::post('/reset-password', [\App\Http\Controllers\AuthController::class, 'resetPassword']);
    Route::get('/articles', [\App\Http\Controllers\ArticleController::class, 'index']);
    Route::get('/article/{id}', [\App\Http\Controllers\ArticleController::class, 'show']);
    Route::get('/categories', [\App\Http\Controllers\CategoryController::class, 'index']);
    Route::post('/add_preferences', [\App\Http\Controllers\CategoryController::class, 'addPref']);
    Route::get('/checkValid',[\App\Http\Controllers\AuthController::class, 'checkTokenValidity']);

});
