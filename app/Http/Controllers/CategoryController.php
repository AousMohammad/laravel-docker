<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Category;
use App\Models\UserCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return response()->json(['categories' => $categories], 200);
    }
    public function addPref(Request $request){
        $ids=$request->ids;
        foreach ($ids as $id){
            $category=Category::find($id);
            if ($category){
                UserCategory::create([
                    'user_id'=>Auth::id(),
                    'category_id'=>$category->id
                ]);
            }
        }
        return response()->json("preferences added successfully");
    }

}
