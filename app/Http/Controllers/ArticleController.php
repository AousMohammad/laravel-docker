<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Author;
use App\Models\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        $query = Article::with('author', 'source', 'category');


        if ($request->has('date')) {
            $query->whereDate('published_at', $request->date);
        }

        if ($request->has('source')) {
            $sources = Source::where('name', 'like', "%{$request->source}%")->pluck('id');
            $query->whereIn('source_id', $sources);
        }
        if ($request->has('author')) {
            $authors = Author::where('name', 'like', "%{$request->author}%")->pluck('id');
            $query->whereIn('author_id', $authors);
        }
        if ($request->has('keyword')) {
            $query->where('title', 'like', "%{$request->keyword}%")
                ->orWhere('description', 'like', "%{$request->keyword}%");
        }

        $query->whereIn('category_id', $user->preferredCategories->pluck('id'));


        $articles = $query->paginate(20);

        return response()->json(['articles' => $articles], 200);
    }

    public function show($id)
    {
        $article = Article::findOrFail($id);
        return response()->json(["data" => $article], 200);
    }
}
