<?php

namespace App\Console\Commands;

use App\Models\Author;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\Article;
use App\Models\Source;
use Illuminate\Support\Facades\Log;

class FetchArticles extends Command
{
    protected $signature = 'fetch:articles';
    protected $description = 'Fetch articles from NewsAPI, Guardian, and New York Times';

    public function handle()
    {
        // Fetch articles from NewsAPI

        $categories=Category::all();
        foreach ($categories as $category) {
            $newsApiClient = new Client();
            $newsApiResponse = $newsApiClient->get('https://newsapi.org/v2/top-headlines', [
                'query' => [
                    'apiKey' => config('secret.NEWSAPI_KEY'),
                    'country' => 'us',
                    'category'=>$category['name']
                ],
            ]);

            $newsArticles = json_decode($newsApiResponse->getBody(), true)['articles'];

            foreach ($newsArticles as $article) {
                $source = Source::firstOrCreate(['name' => $article['source']['name']]);
                $author = Author::firstOrCreate(['name' => $article['author']||"Unkown"]);
                Article::create([
                    'source_id' => $source->id,
                    'title' => $article['title'],
                    'description' => $article['description'],
                    'author_id' => $author->id,
                    'article_url' => $article['url'],
                    'image_url' => $article['urlToImage'],
                    'published_at' => Carbon::parse($article['publishedAt']),
                    'category_id'=>$category['id']
                ]);
            }

        // Fetch articles from Guardian API
            $guardianApiClient = new Client();
            $guardianApiResponse = $guardianApiClient->get('https://content.guardianapis.com/search', [
                'query' => [
                    'api-key' => config('secret.GUARDIANAPI_KEY'),
                    'category'=>$category['name']
                ],
            ]);

            $guardianArticles = json_decode($guardianApiResponse->getBody(), true)['response']['results'];

            foreach ($guardianArticles as $article) {
                $source = Source::firstOrCreate(['name' => $article['sectionName']]);
                Article::create([
                    'source_id' => $source->id,
                    'title' => $article['webTitle'],
                    'article_url'=>$article['webUrl'],
                    'published_at' => Carbon::parse($article['webPublicationDate']),
                    'category_id'=>$category['id']
                ]);
            }

            // Fetch articles from New York Times API
            $nytApiClient = new Client();
            $nytApiResponse = $nytApiClient->get('https://api.nytimes.com/svc/archive/v1/2019/1.json', [
                'query' => [
                    'api-key' => config('secret.NEWYORKAPI_KEY'),
                    'category'=>$category['name']
                ],
            ]);

            $nytArticles = json_decode($nytApiResponse->getBody(), true)['response']['docs'];

            foreach ($nytArticles as $article) {
                $source = Source::firstOrCreate(['name' => 'The New York Times']);
                $author = Author::firstOrCreate(['name' =>  !empty($article['byline']['person'][0]['firstname']) && !empty($article['byline']['person'][0]['lastname'])
                    ? $article['byline']['person'][0]['firstname'] . " " . $article['byline']['person'][0]['lastname']
                    : "Unknown"]);
                Article::create([
                    'source_id' => $source->id,
                    'title' => $article['headline']['main'],
                    'author_id' => $author->id,
                    'article_url' => $article['web_url'],
                    'published_at' => Carbon::parse($article['pub_date']),
                    'category_id'=>$category->id
                ]);
            }
        }
    }
}
